import fetch from "node-fetch";

let search = async (number) => {
    let searchString = "";
    let count = 0
    while (true){
        searchString += number;
        let response = await fetch('https://www.angio.net/newpi/piquery?q='+ searchString);
        let data = await response.json();
        if (data.r[0].c <= 0)
            return count
        count ++;
    }
}
let searchNumber = process.argv[2] // commandline argument. run example: node searchPi.js 9
console.log(`de lengte van de langste string van een opeenvolging ${searchNumber}'s is: ${await search(searchNumber)}`)
